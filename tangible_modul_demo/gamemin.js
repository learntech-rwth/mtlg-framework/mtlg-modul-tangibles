/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2016-12-29T16:33:56+01:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   thiemo
 * @Last modified time: 2017-06-14T18:46:04+02:00
 */

"use strict";

var MTLG = (function(m) {

  var game = (function() {
    var model, view, controller, stage;

    function tangible_model() {
      this.tangibles = [];
      this._freetangibles = [];
      this.touches = [];
    }

    tangible_model.prototype = {

      freeTangibles: function(e) {
        return this.tangibles[e.tangible.id];
      },

      freeTouches: function(e) {
        return this.touches[e.pointerID];
      },

      addtangible: function(e, radius) {


        function tangibleObject(id) {
          this.shape = new createjs.Shape();
          this.additional = new createjs.Shape();
          this.container = new createjs.Container();
          this.id = id;
          this.radius = radius;
        }

        tangibleObject.prototype = {
          // has to be in view after add perhaps
          setAdditional: function(x, y) {
            var pt = this.shape.globalToLocal(x, y);
            this.additional.x = pt.x;
            this.additional.y = pt.y;
          },

          setShape: function(x, y) {
            this.shape.y = y;
            this.shape.x = x;
          },

          setMiddle: function(x, y) {
            this.container.y = y;
            this.container.x = x;
          },

          setRotation: function(degree) {
            this.container.rotation = degree;
          },

          setTouches: function(touches) {
            this.mainTid = touches[0];
            this.secTid = touches[1];
            this.thirdTid = touches[2];
          }
        }

        this.tangibles[e.tangible.id] = this.tangibles[e.tangible.id] || new tangibleObject(e.tangible.id);
        this.tangibles[e.tangible.id].offDisplay = false;

        // set Touches, set middle
        this.tangibles[e.tangible.id].setMiddle(e.tangible.x, e.tangible.y);
        this.tangibles[e.tangible.id].setShape(0, 0);
        this.tangibles[e.tangible.id].mainTouchX = e.tangible.mainTouchX;
        this.tangibles[e.tangible.id].mainTouchY = e.tangible.mainTouchY;
        this.tangibles[e.tangible.id].setRotation(e.tangible.degree);
        this.tangibles[e.tangible.id].radius = radius;

        this.counter.tangibles = this.counter.tangibles + 1;

        return this.tangibles[e.tangible.id];
      },

      mvtangible: function(tan) {
        if (tan.id !== -1) {
          this.tangibles[tan.id].setMiddle(tan.x, tan.y);
          this.tangibles[tan.id].setRotation(tan.degree);
        }
      },

      deltangible: function(tan) {
        if (tan.id !== -1) {
          this.tangibles[tan.id].offDisplay = true;
          this._freetangibles.push(tan.id);
          this.counter.tangibles = this.counter.tangibles - 1;
        }
      },

      addTouches: function(e) {
        var id = e.pointerID;
        var touchObject = function() {
          this.shape = new createjs.Shape();
        };
        if (id !== -1) {
          this.touches[id] = this.touches[id] || new touchObject();
          this.touches[id].shape.x = e.stageX;
          this.touches[id].shape.y = e.stageY;
          this.touches[id].event = e;
          this.touches[id].offDisplay = false;
          this.counter.touches = this.counter.touches + 1;
          return this.touches[id];
        }
        return false;
      },

      mvTouches: function(e) {
        var id = e.pointerID;
        if (id !== -1) {
          this.touches[id].shape.x = e.stageX;
          this.touches[id].shape.y = e.stageY;
        }
      },

      getTouches: function() {
        return this.touches;
      },

      delTouches: function(tid) {
        if (tid !== -1) {
          this.touches[tid].offDisplay = true;
          this.counter.touches = this.counter.touches - 1;
        }
      },

      addCounter: function() {
        function counter() {
          this.touches = 0;
          this.tangibles = 0;
          this.preText = "Touches: ";
          this.preText2 = "Tangibles: ";
          this.shape = new createjs.Text("", "48px Arial", "FFFFFF");
          this.shape.x = 50;
          this.shape.y = stage.canvas.height - 150;
        }

        this.counter = new counter();
      },

      mvCounter: function() {
        this.counter.shape.y = stage.canvas.height - 150;
      },

      updateCounter: function() {
        this.counter.shape.text = this.counter.preText + this.counter.touches + "\n" + this.counter.preText2 + this.counter.tangibles;
      }
    };

    function tangible_view(model, stage) {
      this._model = model;
      this._stage = stage;
    }

    tangible_view.prototype = {

      addContainer: function() {
        var background = new createjs.Shape();
        var label = new createjs.Text("Filter Touches", "bold 24px Arial", "#FFFFFF");
        var regExContainer = new createjs.Container();

        background.name = "background";
        background.graphics.beginFill("red").drawRoundRect(0, 0, 180, 60, 10);

        label.name = "label";
        label.textAlign = "center";
        label.textBaseline = "middle";
        label.x = 180 / 2;
        label.y = 60 / 2;

        regExContainer.name = "cRegEx";
        regExContainer.x = Math.random() * stage.canvas.width || 100;
        regExContainer.y = Math.random() * stage.canvas.height || 100;
        regExContainer.regX = 90;
        regExContainer.regY = 30;
        regExContainer.rotation += 130;
        regExContainer.state = false;

        regExContainer.addChild(background, label);
        regExContainer.on("pressmove", draggable);
        regExContainer.on("click", handleContainerDown);

        function draggable(e) {
          this.x = e.stageX;
          this.y = e.stageY;
        }

        function handleContainerDown(e) {
          if (this.state) {
            this.children[0].graphics.beginFill("green").drawRoundRect(0, 0, 180, 60, 10);
            this.state = false;
          }
          else {
            this.children[0].graphics.beginFill("red").drawRoundRect(0, 0, 180, 60, 10);
            this.state = true;
          }
          MTLG.tangible.filterTouches(this.state);
        }

        stage.addChild(regExContainer);
      },

      addTangibleToStage: function(tangible) {
        tangible.container.addChild(tangible.shape);
        tangible.container.addChild(tangible.additional);
        this._stage.addChild(tangible.container);
      },

      drawtangible: function(tangible) {
        if (tangible) {
          tangible.additional.graphics.beginFill("green").drawCircle(0, 0, tangible.radius / 3).endFill();
          tangible.shape.graphics.beginFill("red").drawCircle(0, 0, tangible.radius).endFill();
          console.log(tangible.radius);
          tangible.setAdditional(tangible.mainTouchX, tangible.mainTouchY);
        }
      },

      cleartangibles: function() {
        var tangibles = model.tangibles;
        tangibles.forEach(function(t) {
          if (t.offDisplay) {
            t.shape.graphics.clear();
            t.additional.graphics.clear();
          }
        });
      },

      addTouchToStage: function(touch) {
        this._stage.addChild(touch.shape);
      },

      drawtouch: function(touch) {
        touch.shape.graphics.beginFill("blue").drawCircle(0, 0, 40).endFill();
      },

      cleartouches: function() {
        var touches = model.touches;
        touches.forEach(function(t) {
          if (t.offDisplay) {
            t.shape.graphics.clear();
          }
        });
      },

      addText: function() {
        var text;
        this._model.addCounter();
        text = this._model.counter.shape;
        this._stage.addChild(text);
      },

      resize: function() {
        model.mvCounter();
        console.log("resize");
      },

      show: function() {

        this._model.updateCounter();
        this._stage.update();
      }
    };

    function Controller(model, view) {

      var handleTangibleMove = function(e) {
        model.mvtangible(e.tangible);
      };

      var handleTangibleDown = function(e) {
        var free, tangible, radius;
        console.log("tangible event down");
        radius = e.tangible.radius;
        free = model.freeTangibles(e);
        tangible = model.addtangible(e, radius);
        if (!free) {
          view.addTangibleToStage(tangible);
        }
        view.drawtangible(tangible);
      };

      var handleTangibleUp = function(e) {
        model.deltangible(e.tangible);
        view.cleartangibles();
        console.log("tangible event up");
      };

      var handleMouseDown = function(e) {
        var free, touch;

        if (e.pointerID !== -1) {
          if (e.pointerID < 1000) {
            free = model.freeTouches(e);
            touch = model.addTouches(e);

            if (!free) {
              view.addTouchToStage(touch);
            }
            view.drawtouch(touch);
          } else {
            handleTangibleDown(e.nativeEvent);
          }
        }
      };

      var handleMouseMove = function(e) {
        if (e.pointerID !== -1) {
          if (e.pointerID < 1000) {
            model.mvTouches(e);
          } else {
            handleTangibleMove(e.nativeEvent);
          }
        }
      };

      var handleMouseUp = function(e) {
        if (e.pointerID !== -1) {
          if (e.pointerID < 1000) {
            model.delTouches(e.pointerID);
            view.cleartouches();
          } else {
            handleTangibleUp(e.nativeEvent);
          }
        }
      };

      var ticker = function(e) {
        if (!e.paused) {
          view.show();
        }
      };

      //createjs.Touch.enable(view._stage);
      createjs.Ticker.addEventListener("tick", ticker);

      view.addContainer();
      view.addText();

      view._stage.mouseEnabled = false;

      view._stage.on("stagemousedown", handleMouseDown, false);
      view._stage.on("stagemouseup", handleMouseUp, false);
      view._stage.on("stagemousemove", handleMouseMove, false);

      MTLG.tangible.filterTouches(true);
    }

    function init(gamestage) {
      stage = gamestage;

      model = new tangible_model();
      view = new tangible_view(model, stage);
      controller = new Controller(model, view);
    }

    function resize() {
      if (view) {
        view.resize();
      }
    }

    return {
      init: init,
      resize_game: resize
    }
  })();

  m.game = game;
  return m;
})(MTLG);
